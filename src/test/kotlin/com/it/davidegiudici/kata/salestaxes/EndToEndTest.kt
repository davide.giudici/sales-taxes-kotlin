package com.it.davidegiudici.kata.salestaxes

import org.junit.jupiter.api.Test

class EndToEndTest {

    private val basketProductPayService = BasketProductPayService()

    @Test
    fun `example purchase  1`() {
        /*
            ##### Basket
            1 book at 12.49
            1 music CD at 14.99
            1 chocolate bar at 0.85
            ##### Receipt
            1 book: 12.49
            1 music CD: 16.49
            1 chocolate bar: 0.85
            Sales Taxes: 1.50
            Total: 29.83
         */
        val products = listOf(
            prd("book", "12.49", Category.Book),
            prd("music CD", "14.99", Category.Other),
            prd("chocolate bar", "0.85", Category.Food),
        ).map { BasketItem(it, ProductQuantity(1)) }

        val receipt = basketProductPayService.buildReceipt(Basket(products))

        assertBigDecimalEquals("12.49", receipt.items[0].totalPrice)
        assertBigDecimalEquals("16.49", receipt.items[1].totalPrice)
        assertBigDecimalEquals("0.85", receipt.items[2].totalPrice)

        assertBigDecimalEquals("1.50", receipt.totalTaxes)
        assertBigDecimalEquals("29.83", receipt.totalPrice)
    }

    @Test
    fun `example purchase  2`() {
        /*
            ##### Basket
            1 imported box of chocolates at 10.00
            1 imported bottle of perfume at 47.50
            ##### Receipt
            1 imported box of chocolates: 10.50
            1 imported bottle of perfume: 54.65
            Sales Taxes: 7.65
            Total: 65.15
         */
        val products = listOf(
            imprPrd("imported box of chocolates", "10.00", Category.Food),
            imprPrd("imported bottle of perfume", "47.50", Category.Other),
        ).map { BasketItem(it, ProductQuantity(1)) }

        val receipt = basketProductPayService.buildReceipt(Basket(products))

        assertBigDecimalEquals("10.50", receipt.items[0].totalPrice)
        assertBigDecimalEquals("54.65", receipt.items[1].totalPrice)

        assertBigDecimalEquals("7.65", receipt.totalTaxes)
        assertBigDecimalEquals("65.15", receipt.totalPrice)
    }

    @Test
    fun `example purchase  3`() {
        /*
            ##### Basket
            1 imported bottle of perfume at 27.99
            1 bottle of perfume at 18.99
            1 packet of headache pills at 9.75
            1 box of imported chocolates at 11.25
            ##### Receipt
            1 imported bottle of perfume: 32.19
            1 bottle of perfume: 20.89
            1 packet of headache pills: 9.75
            1 imported box of chocolates: 11.85
            Sales Taxes: 6.70
            Total: 74.68
         */
        val products = listOf(
            imprPrd("imported bottle of perfume", "27.99", Category.Other),
            prd("bottle of perfume", "18.99", Category.Other),
            prd("packet of headache pills", "9.75", Category.Medical),
            imprPrd("box of imported chocolates", "11.25", Category.Food),
        ).map { BasketItem(it, ProductQuantity(1)) }

        val receipt = basketProductPayService.buildReceipt(Basket(products))

        assertBigDecimalEquals("32.19", receipt.items[0].totalPrice)
        assertBigDecimalEquals("20.89", receipt.items[1].totalPrice)
        assertBigDecimalEquals("9.75", receipt.items[2].totalPrice)
        assertBigDecimalEquals("11.85", receipt.items[3].totalPrice)

        assertBigDecimalEquals("6.70", receipt.totalTaxes)
        assertBigDecimalEquals("74.68", receipt.totalPrice)
    }
}
