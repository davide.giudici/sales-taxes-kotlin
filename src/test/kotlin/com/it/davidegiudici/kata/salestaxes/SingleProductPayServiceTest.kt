package com.it.davidegiudici.kata.salestaxes

import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory

class SingleProductPayServiceTest {

    private val singleProductPayService = SingleProductPayService()

    @Test
    fun `StandardProduct of a exempt category, has ZERO taxes`() {
        val shelfPrice = bd("5.0")
        val product = StandardProduct("something", shelfPrice, Category.Book)
        val receiptProduct = singleProductPayService.buildReceipt(product)

        assertBigDecimalEquals("0", receiptProduct.tax)
        assertBigDecimalEquals(shelfPrice, receiptProduct.price)
    }

    @Test
    fun `StandardProduct of a NON exempt category, has 10x100 of taxes`() {
        val shelfPrice = bd("10.0")
        val product = StandardProduct("something", shelfPrice, Category.Other)
        val receiptProduct = singleProductPayService.buildReceipt(product)

        assertBigDecimalEquals("1", receiptProduct.tax)
        assertBigDecimalEquals("11", receiptProduct.price)
    }

    @Test
    fun `ImportedProduct of a exempt category, has 5x100 of taxes`() {
        val shelfPrice = bd("10.0")
        val product = ImportedProduct("something", shelfPrice, Category.Food)
        val receiptProduct = singleProductPayService.buildReceipt(product)

        assertBigDecimalEquals("0.5", receiptProduct.tax)
        assertBigDecimalEquals("10.5", receiptProduct.price)
    }

    @Test
    fun `ImportedProduct of a NON exempt category, has (5+10)x100 of taxes`() {
        val shelfPrice = bd("10.0")
        val product = ImportedProduct("something", shelfPrice, Category.Other)
        val receiptProduct = singleProductPayService.buildReceipt(product)

        assertBigDecimalEquals("1.5", receiptProduct.tax)
        assertBigDecimalEquals("11.5", receiptProduct.price)
    }
}
