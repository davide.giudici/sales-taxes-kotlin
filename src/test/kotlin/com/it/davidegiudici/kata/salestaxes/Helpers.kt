package com.it.davidegiudici.kata.salestaxes

import java.math.BigDecimal
import kotlin.test.assertTrue

fun prd(descr: String, shelfPrice: String, category: Category) =
    StandardProduct(descr, bd(shelfPrice), category)

fun imprPrd(descr: String, shelfPrice: String, category: Category) =
    ImportedProduct(descr, bd(shelfPrice), category)

fun bd(str: String) = BigDecimal(str)

fun assertBigDecimalEquals(expected: String, actual: BigDecimal) =
    assertBigDecimalEquals(bd(expected), actual)

fun assertBigDecimalEquals(expected: BigDecimal, actual: BigDecimal) =
    assertTrue(0 == expected.compareTo(actual), "Expected <$expected>, actual <$actual>.")

