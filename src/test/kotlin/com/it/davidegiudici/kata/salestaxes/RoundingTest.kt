package com.it.davidegiudici.kata.salestaxes

import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import java.math.BigDecimal
import kotlin.test.assertTrue

class RoundingTest {

    @TestFactory
    fun `taxes are rounded up to the nearest 0,05`() =
        listOf(
            "1.00" to "1.00",
            "1.01" to "1.05",
            "1.04" to "1.05",
            "1.05" to "1.05",
            "1.06" to "1.10",
            "1.07" to "1.10",
            "1.08" to "1.10",
            "1.30" to "1.30",
        ).map { (input, output) ->
            DynamicTest.dynamicTest("rounding tax of $input result to $output") {

                val result = bd(input).roundAsTax()

                assertBigDecimalEquals(output, result)
            }
        }
}
