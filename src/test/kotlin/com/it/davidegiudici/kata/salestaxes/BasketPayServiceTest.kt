package com.it.davidegiudici.kata.salestaxes

import org.junit.jupiter.api.Test

class BasketPayServiceTest {

    private val basketProductPayService = BasketProductPayService()

    @Test
    fun `the total shelf price of a basket is the sum of the shelf price of the product inside`() {
        val products = listOf(
            imprPrd("imported bottle of perfume", "27.99", Category.Other),
            prd("bottle of perfume", "18.99", Category.Other),
        ).map { BasketItem(it, ProductQuantity(2)) }

        val basket = Basket(products)

        assertBigDecimalEquals("93.96", basket.totalShelfPrice)
        assertBigDecimalEquals("93.96", (basket.items[0].product.shelfPrice + basket.items[1].product.shelfPrice).multiply(bd("2")))
    }

    @Test
    fun `the receipt total taxes is the sum of the taxes of the product inside`() {
        val products = listOf(
            imprPrd("imported bottle of perfume", "27.99", Category.Other),
            prd("bottle of perfume", "18.99", Category.Other),
        ).map { BasketItem(it, ProductQuantity(1)) }

        val receipt = basketProductPayService.buildReceipt(Basket(products))

        assertBigDecimalEquals("6.10", receipt.totalTaxes)
        assertBigDecimalEquals("6.10", receipt.items[0].totalTaxes + receipt.items[1].totalTaxes)
    }

    @Test
    fun `the receipt total taxes is double of the taxes of single product inside if I buy quantity 2`() {
        val products = listOf(
            imprPrd("imported bottle of perfume", "27.99", Category.Other),
        ).map { BasketItem(it, ProductQuantity(2)) }

        val receipt = basketProductPayService.buildReceipt(Basket(products))

        assertBigDecimalEquals("8.40", receipt.totalTaxes)
        assertBigDecimalEquals("8.40", receipt.items[0].product.tax * bd("2"))
    }
}
