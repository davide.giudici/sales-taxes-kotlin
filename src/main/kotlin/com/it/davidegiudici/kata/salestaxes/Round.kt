package com.it.davidegiudici.kata.salestaxes

import java.math.BigDecimal
import java.math.RoundingMode

const val ROUND_TO = "0.05"

fun BigDecimal.roundAsTax(): BigDecimal {
    return roundUpTo(this, BigDecimal(ROUND_TO), RoundingMode.UP)
}

fun roundUpTo(
    value: BigDecimal,
    increment: BigDecimal,
    roundingMode: RoundingMode?
): BigDecimal = if (increment.signum() != 0)
    value
        .divide(increment, 0, roundingMode)
        .multiply(increment)
else
    value