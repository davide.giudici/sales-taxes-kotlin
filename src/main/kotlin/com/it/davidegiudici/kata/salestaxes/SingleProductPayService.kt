package com.it.davidegiudici.kata.salestaxes

import java.math.BigDecimal

class SingleProductPayService {

    fun buildReceipt(product: Product): ReceiptProduct {
        return ReceiptProduct(product, computeTax(product))
    }

    private fun computeTax(product: Product): BigDecimal {
        return product.shelfPrice
            .multiply(BigDecimal(product.taxRate))
            .divide(BigDecimal(100))
            .roundAsTax()
    }
}
