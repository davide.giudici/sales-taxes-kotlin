package com.it.davidegiudici.kata.salestaxes

import java.math.BigDecimal

class ReceiptProduct(
    product: Product,
    val tax: BigDecimal
) : Product by product {
    val price: BigDecimal = shelfPrice + tax
}
