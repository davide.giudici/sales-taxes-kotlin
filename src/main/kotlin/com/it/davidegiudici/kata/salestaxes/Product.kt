package com.it.davidegiudici.kata.salestaxes

import java.math.BigDecimal

interface Product {
    val taxRate: Double
    val description: String
    val shelfPrice: BigDecimal
    val category: Category
}

data class StandardProduct(
    override val description: String,
    override val shelfPrice: BigDecimal,
    override val category: Category
) : Product {
    override val taxRate = category.taxRate
}

private const val TAX_IMPORTED_RATE = 5.0

data class ImportedProduct(
    override val description: String,
    override val shelfPrice: BigDecimal,
    override val category: Category
) : Product {
    override val taxRate = category.taxRate + TAX_IMPORTED_RATE
}
