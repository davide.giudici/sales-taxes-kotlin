package com.it.davidegiudici.kata.salestaxes

import java.math.BigDecimal

class BasketProductPayService {
    private val singleProductPayService = SingleProductPayService()

    fun buildReceipt(product: Basket): Receipt =
        product
            .items.map {
                val receiptPrd = singleProductPayService
                    .buildReceipt(it.product)

                ReceiptItem(receiptPrd, it.quantity)
            }
            .let {
                Receipt(it)
            }

}

class Receipt(val items: List<ReceiptItem>) {
    val totalPrice: BigDecimal = items.sumOf { it.totalPrice }
    val totalTaxes: BigDecimal = items.sumOf { it.totalTaxes }
}

class ReceiptItem(val product: ReceiptProduct, val quantity: ProductQuantity) {
    val totalPrice = quantity multiply product.price
    val totalTaxes = quantity multiply product.tax
}

class Basket(val items: List<BasketItem>) {
    val totalShelfPrice: BigDecimal = items.sumOf { it.totalShelfPrice }
}

data class BasketItem(val product: Product, val quantity: ProductQuantity) {
    val totalShelfPrice: BigDecimal = quantity multiply product.shelfPrice
}

@JvmInline
value class ProductQuantity(private val value: Int) {
    infix fun multiply(bd: BigDecimal): BigDecimal = bd.multiply(BigDecimal(value))
}
