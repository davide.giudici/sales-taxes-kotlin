package com.it.davidegiudici.kata.salestaxes

private const val NOT_EXEMPT_TAX_RATE = 10.0
private const val EXEMPT_TAX_RATE = 0.0

enum class Category(val taxRate: Double) {
    Book(EXEMPT_TAX_RATE),
    Food(EXEMPT_TAX_RATE),
    Medical(EXEMPT_TAX_RATE),
    Other(NOT_EXEMPT_TAX_RATE)
}
